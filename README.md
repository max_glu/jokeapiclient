# Android Client for Joke API 
Just a simple pet project to have something to demonstrate and play with. 
Based on [JokeAPI](https://v2.jokeapi.dev/)

## Disclaimer
Please be noted, that I use 3rd party API which is not under my control. 
Some content can be not appropriate for some people. I am going to use strict mode to avoid offensive jokes, but it is not guaranteed even by API. 

## Current state
Now it is under development. Most fresh version in MVP branch. Version is not stable yet.
If you want to see how application works, but don't want to build it, you can check [this video on YouTube](https://youtu.be/JFLKyqksFeY).

The main goal for the first version is to have a working prototype that can show jokes with filters.
The joke can be changed by swipe.
UI for first version will be Compose-based. 
Architecture based on recommended Google approach. You can [check it here](https://developer.android.com/jetpack/guide)

## Global plans
- Add unit tests
- Advanced UI animations
- Saving filters, settings and jokes to persistance storage
- Rewrite UI logic to Classic Views

## Used and planned to use technologies 
### Now:
- Kotlin
- Coroutines
- Hilt
- Jackson
- Retrofit
- Jetpack Compose
- Compose Navigation
- Jetpack Architecture
- Timber

### Planned:
- Mockk
- Data Store
- Room
- etc..
