package com.maxglu.androidjokeapiclient.ui.screens.randomjoke

import androidx.compose.foundation.layout.Column
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import com.maxglu.androidjokeapiclient.Greeting

@Composable
fun RandomJokeScreen(navController: NavController) {
    Surface(color = MaterialTheme.colors.background) {
        Column {
            Greeting("from Second screen!")
            TextButton(onClick = { navController.popBackStack() }) {
                Text(text = "Go back")
            }
        }
    }
}