package com.maxglu.androidjokeapiclient.ui.screens

sealed class Screen(val route: String) {
    object Main : Screen("main")
    object RandomJoke : Screen("random_joke")
}