package com.maxglu.androidjokeapiclient.ui.screens.main

import androidx.compose.foundation.layout.Column
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
x
@Composable
fun MainScreen(navController: NavController) {
    Surface(color = MaterialTheme.colors.background) {
        Column {
            Greeting("from First screen!")
            TextButton(onClick = { navController.navigate("random_joke") }) {
                Text(text = "Go to next screen")
            }
        }
    }
}